<?php

declare( strict_types = 1 );
namespace FixBrokenImages
{
	/*
	Plugin Name:  Fix Broken Images
	Plugin URI:   https://www.jaimeson-waugh.com
	Description:  Allows one to check and automatically fix broken images in posts.
	Version:      1.0
	Author:       Jaimeson Waugh
	Author URI:   https://www.jaimeson-waugh.com
	License:      GPL2
	License URI:  https://www.gnu.org/licenses/gpl-2.0.html
	Text Domain:  fix-broken-images
	Domain Path:  /languages

	"Fix Broken Images" is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	"Fix Broken Images" is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with "Fix Broken Images". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
	*/

	require_once( 'fix-elements.php' );
	class FixBrokenImages extends FixElements
	{
		//
		//  PUBLIC
		//
		/////////////////////////////////////////////////////////

			public function __construct()
			{
				parent::__construct( 'Broken Images', 'broken-images' );
			}

			protected function CheckForBrokenElement( $image ) : ?array
			{
				$src = $image->getAttribute( 'src' );
				if ( $src )
				{
					if ( $this->TestElementIsBroken( $src ) )
					{
						return [ 'dom' => $image, 'src' => $src ];
					}
				}
				return null;
			}

			protected function ChangeElement( $element ) : void
			{
				$element[ 'dom' ]->parentNode->removeChild( $element[ 'dom' ] );
			}
	}
	new FixBrokenImages();

}
